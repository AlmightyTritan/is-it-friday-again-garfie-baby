import { Component, OnInit } from '@angular/core'
import { Observable, timer } from 'rxjs'
import { map, distinctUntilChanged } from 'rxjs/operators'
import { Meta } from '@angular/platform-browser'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  isItFridayGarfieBaby$: Observable<Boolean>

  constructor(private _meta: Meta) { }

  /**
   * Jon storms into the fucking room, racing up to his beloved feline companion. He arches his head back,
   * unhinges his jaw, and preps to bellow. If the day of the week is not Friday Garfield passes his stat check
   * and forces the gaping maw shut.
   *
   * @return  Returns TRUE if the day is Friday
   */
  ngOnInit() {
    this.isItFridayGarfieBaby$ = timer(0, 1000).pipe(
      map(() => new Date().getDay() === 4),
      distinctUntilChanged()
    )

    this.isItFridayGarfieBaby$.subscribe(itIs => {
      let title = itIs ?
        'friday again, garfie baby' :
        'you absolute cretin, you buffoon, you fool, did you really think it was friday? how embarassing'
      let image = itIs ?
          'https://isitfridayagaingarfiebaby.bigweed.life/assets/friday.jpg' :
          'https://isitfridayagaingarfiebaby.bigweed.life/assets/shut-the-fuck-up-jon.jpg'

      this._meta.updateTag({ property: 'og:title', content: title })
      this._meta.updateTag({ property: 'twitter:text:title', content: title })
      this._meta.updateTag({ property: 'og:image', content: image })
      this._meta.updateTag({ property: 'og:image:secure_url', content: image })
      this._meta.updateTag({ property: 'twitter:image', content: image })
    })
  }

}
